from wakeonlan import send_magic_packet
import time
import requests

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
#urllib3.disable_warnings(urllib3.exceptions.HTTPSConnection)


def internet_online():
    try:
        response = requests.get('https://192.168.1.163', verify=False)
        if (response.status_code == 200):
          return True
        else:
          return False
    except Exception as err: 
        print("EXCEPTION: {0}".format(err))
        return False



while( True ):
   if ( internet_online() == True ):
      print("Server online. Not sending packet.")
   else:
      try:

         print("Sending Wake on lan to MINIPC")
         # ethernet - 192.168.1.91
         send_magic_packet('B0-41-6F-0A-C4-09')
 
         # wi-fi connection (dhcp)
         #send_magic_packet('58-1C-F8-A7-9B-BC')
      except Exception as ex:
         print("Error sending WOL packet.")


   # give the ol pi a rest
   time.sleep(5)
   

