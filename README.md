# pi_wake-on-lan

I have a minipc that I use for staging development and a couple of other purposes. However, it's winblows based and the lil shit has a tendancy to turn off 'at random'. I have an old Raspberry Pi laying around so I made a simple service to keep staging alive. The service checks to see if the local ip is up and sends WOL (Wake on lan) if it's not online. I used the local ip incase the domainname is pointed somewhere else. Check do work, sleep. Over and over. EZ-Peeze

I need/want to use a .env file to make it easier to manage, but this will do for now.


## Getting started

Building a simple WOL service from https://pypi.org/project/wakeonlan/ to wake MAC: D8:BB:C1:CB:26:00

sudo cp wol-minipc01.service /lib/systemd/system

![alt text](<./wol-minipc01_service.png>)
